/**
 * simple node script to check that the local package version is different from the published one
 */ 
const info = require('package-info')

// read the local package info
const local = require('./package.json')

// read the published package info
info(local.name).then(published => {
  if (published.version === local.version) {
    console.warn(
      "The npm package version is identical to the previously published package. " + 
      "Increase the version number in package.json"
    );
    // exit with an error if the versions match
    process.exit(1)
  }
})
