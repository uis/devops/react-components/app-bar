### Examples

```js
<AppBar position="static" />
```

```js
<AppBar position="static">
  <div>this is some <em>custom</em> content</div>
</AppBar>
```